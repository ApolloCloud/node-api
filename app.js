const express = require('express')
const app = express()
const port = 3000

app.get('/', (req, res) => {
    res.send('Welcome to our demo Node API!')
}
)

app.get('/fibonacci/:value', (req, res) => {
        var fibValue = parseInt(req.params['value'])
        var result = fib(fibValue);
        res.send('The result is: ' + result)
    }
)

app.get('/load-test/:severity', (req, res) => {
    var fibValue = parseInt(req.params['severity'])
     
    console.log("Start!")
    var i = 0;
    for( i = 0; i < 1000 ; i++){
        console.log('Index: ' + i)
        fib(fibValue)
    }
    console.log("End!")

    res.send('Load Test!')
})

app.listen(port, () => console.log(`Example app listening on port ${port}!`))

// --- Fibonacci Function ---
// *** Gets you the nth fibonacci value in the fibonacci sequence ***
function fib(i) {
    if(i === 0){
        return 0
    }else if (i === 1){
        return 1
    }else{
        return fib(i - 2) + fib(i - 1); 
    }         
  }