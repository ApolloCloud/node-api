#Install git
sudo yum install git

#Clone Project
git clone https://gitlab.com/ApolloCloud/node-api.git

#Install Node
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.32.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install 10.15.0

# Traverse to project
cd ./node-api

# Install dependencies
npm install 

#Install pm2
npm install pm2 -g

#Start application
pm2 start app.js