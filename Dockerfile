FROM node:8.15-alpine
 
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
 
COPY package.json ./
RUN npm install
 
#  Copies all folders within Project Direction
COPY . .
 
EXPOSE 3000
CMD ["npm", "start"]